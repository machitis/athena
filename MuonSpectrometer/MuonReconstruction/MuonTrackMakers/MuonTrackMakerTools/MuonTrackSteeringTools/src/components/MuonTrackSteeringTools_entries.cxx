#include "../MuonTrackSteering.h"
#include "../MuPatCandidateTool.h"
#include "../MuPatHitTool.h"
#include "../MooCandidateMatchingTool.h"
#include "../MooTrackFitter.h"
#include "../MooTrackBuilder.h"
#include "../MuonTrackSelectorTool.h"

using namespace Muon;

DECLARE_COMPONENT( MuonTrackSteering )
DECLARE_COMPONENT( MuPatHitTool )
DECLARE_COMPONENT( MuPatCandidateTool )
DECLARE_COMPONENT( MooCandidateMatchingTool )
DECLARE_COMPONENT( MooTrackFitter )
DECLARE_COMPONENT( MooTrackBuilder )
DECLARE_COMPONENT( MuonTrackSelectorTool )
