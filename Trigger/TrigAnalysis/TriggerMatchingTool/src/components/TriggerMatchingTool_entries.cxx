#include "TriggerMatchingTool/MatchingTool.h"
#include "TriggerMatchingTool/IParticleRetrievalTool.h"
#include "TriggerMatchingTool/MatchFromCompositeTool.h"
#include "TriggerMatchingTool/R3MatchingTool.h"

DECLARE_COMPONENT(Trig::MatchingTool)

DECLARE_COMPONENT(Trig::MatchingTool)
DECLARE_COMPONENT(Trig::IParticleRetrievalTool)
DECLARE_COMPONENT(Trig::MatchFromCompositeTool)
DECLARE_COMPONENT(Trig::R3MatchingTool)

#include "../TestMatchingToolAlg.h"
DECLARE_COMPONENT(TestMatchingToolAlg)
